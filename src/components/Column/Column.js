//Не забудьте подключить CSS-файл к компоненту
//https://monsterlessons.com/project/lessons/react-css-komponentyj-podhod

/**
 * Обратите внимание на структуру нашего проекта
 */

/**
 * Компонент колонки. Должен принимать пропсы name, cards, isDisabled
 * Мы должны отображать name как имя колонки;
 * Мы должны отображать все карточки в колонке (cards);
 * Если передан пропс isCardsHidden мы должны отображать только имя колонки без карточек;
 * isCardsHidden по дефолту должен быть со значением false
 */
export function Column() {
  const renderCards = () => {
    /**
     * Метод рендеринга карточек -- это хорошая практика, выносить сложный рендеринг в отдельную функцию
     * Учтите, что каждая карточка должна иметь уникальный key
     * https://ru.reactjs.org/docs/lists-and-keys.html
     */
  };

  return null;
}
